#include "TFile.h"

#include "RooAbsReal.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooMinimizer.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
#include "RooStats/ModelConfig.h"

#include <iostream>

int main (int argc, char **argv) {
  std::unique_ptr<TFile> in = std::make_unique<TFile>("FitExample_combined_FitExample_model.root", "read");
  auto* ws = dynamic_cast<RooWorkspace*>(in->Get("combined"));

  auto* data = dynamic_cast<RooDataSet*>(ws->data("obsData"));
  RooStats::ModelConfig* mc = dynamic_cast<RooStats::ModelConfig*>(ws->obj("ModelConfig"));
  RooSimultaneous* simPdf = static_cast<RooSimultaneous*>(mc->GetPdf());
  const RooArgSet* glbObs = mc->GetGlobalObservables();
  std::unique_ptr<RooAbsReal> nll(simPdf->createNLL(*data,
                                RooFit::Constrain(*mc->GetNuisanceParameters()),
                                RooFit::GlobalObservables(*glbObs),
                                RooFit::Offset(kTRUE),
                                NumCPU(1, RooFit::Hybrid),
                                RooFit::Optimize(kTRUE)));

  for (int ipoint = 0; ipoint < 10; ++ipoint) {
    std::cout << "ipoint: " << ipoint << "\n";
    RooMinimizer m(*nll);
    m.migrad();
  }

  return 0;
}
