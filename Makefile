BASEDIR  = .
ROOT=-lTreePlayer -lRooFitCore -lRooStats -lHistFactory `root-config --cflags --libs`
ROOTINC= `root-config --cflags`
FLAGS=-g -Wall -ggdb -O3 -fsanitize=address
INCDIR=$(PWD)
LIBS=$(ROOT)

all: example

example.o:example.cxx
	g++ $(FLAGS) -c -o $@ $< $(ROOTINC)

example:example.o
	g++ $(FLAGS) -o $@ example.o ${LIBS}
